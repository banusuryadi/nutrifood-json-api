<?php

class ApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testShouldUpdateReference()
    {
        $this->json('POST', '/update/1', ['batch' => 'ABC123', 'product' => '12/12/2019', 'expired' => '12/12/2020', 'layout' => 0])
            ->seeJson(['success' => true]);
    }

    public function testShouldReturnReference()
    {
        $response = $this->get('/data');
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
               '*' => [
                    'id',
                    'batch',
                    'product',
                    'expired',
                    'layout',
                    'created_at'
               ]
            ]);
    }
}
