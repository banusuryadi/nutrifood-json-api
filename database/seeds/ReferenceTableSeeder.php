<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReferenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('references')->insert([
            [
                'id' => 1,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 2,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 3,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 4,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 5,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 6,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 7,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 8,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 9,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 10,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 11,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 12,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 13,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 14,
                'created_at' => date('Y:m:d H:i:s')
            ],[
                'id' => 15,
                'created_at' => date('Y:m:d H:i:s')
            ]
        ]);
    }
}
