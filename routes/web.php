<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

$router->post('/update/{id}', function (Request $request, $id) use ($router) {
    $token = $request->input('token');
    if(is_null($token) || $token != env('APP_TOKEN')){
        return response('Unauthorized.', 401);        
    }
    $batch = $request->input('batch') ?: null;
    $product = $request->input('product') ?: null;
    $expired = $request->input('expired') ?: null;
    $layout = $request->input('layout') ?: null;
    
    $affected = DB::table('references')->where('id', $id)->update([
        'batch' => $batch,
        'product' => $product,
        'expired' => $expired,
        'layout' => $layout,
        'created_at' => date('Y:m:d H:i:s')
    ]);
    if ($affected < 1){
        return response()->json(['success' => false, 'id' => $id, 'msg' => 'Failed to update id '.$id]);
    }
    return response()->json(['success' => true, 'msg' => 'Successfully update id: '.$id]);
});

$router->get('/data', function (Request $request) use ($router) {
    $results = app('db')->select("select * from `references`");
    return response()->json($results);
});

$router->get('/add/{id}', function(Request $request, $id) use ($router) {
    $token = $request->input('token');
    if(is_null($token) || $token != env('APP_TOKEN')){
        return response('Unauthorized.', 401);        
    }
    try {
        $rowId = DB::table('references')->insertGetId(['id' => $id]);
        return response()->json(['success' => true, 'msg' => 'Successfully add id: '.$rowId]);
    } catch (\Throwable $th) {
        return response()->json(['success' => false, 'id' => $id, 'msg' => 'Failed to add id '.$id]);
    }
});

$router->get('/remove/{id}', function(Request $request, $id) use ($router){
    $token = $request->input('token');
    if(is_null($token) || $token != env('APP_TOKEN')){
        return response('Unauthorized.', 401);        
    }
    $affected = DB::table('references')->where('id', $id)->delete();
    if($affected < 1){
        return response()->json(['success' => false, 'id' => $id, 'msg' => 'Failed to remove id '.$id]);
    }
    return response()->json(['success' => true, 'id' => $id, 'msg' => 'Successfully remove id: '.$id]);

});