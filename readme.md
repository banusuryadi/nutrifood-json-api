# **SIMPLE JSON API**
## Requirement
 - PHP >= 7.2
 - Sqlite3 PHP Extension
 - OpenSSL PHP Extension
 - PDO PHP Extension
 - Mbstring PHP Extension
## Installation
 - `git clone`
 - `composer install`
 - For Linux System: `chmod -R 775` and `chown -R :www-data` *(for apache/nginx)* to `/storage` folder
    - Note: that `chown` depends on what :group that run the web server *(e.g apache/nginx)*
 - Set `/public` as directory root on the web server *(e.g apache/nginx)*
 ## Configuration
 - Create or copy `.env.exampe` to app root folder as `.env`.
 - Modify `.env` file with:
 ```
 APP_NAME=Lumen
 APP_ENV=local
 APP_KEY=yourappkey
 APP_DEBUG=true
 APP_URL=http://yourapppurl
 APP_TIMEZONE=UTC
 APP_TOKEN=881243 #EDIT WITH YOUR TOKEN
 
 LOG_CHANNEL=stack
 LOG_SLACK_WEBHOOK_URL=
 
 DB_CONNECTION=sqlite
 DB_DATABASE=../storage/database.sqlite
 
 CACHE_DRIVER=file
 QUEUE_CONNECTION=sync
 ```
## API Usage
### **ADDING NEW OBJECT**
*GET* :  `/add/{id}`

Required Parameters:
```
token: int
```

Example Responses:
```json
// Success
{
    "success": true,
    "msg": "Successfully add id: 15"
}

// Failed
{
    "success": false,
    "id": "15",
    "msg": "Failed to add id 15"
}
```
### **RETREIVING ALL OBJECTS**
*GET* : `/data`
Example Responses:
```json
[
    {
        "id": "1",
        "batch": "ABC1223",
        "product": "12/12/2019",
        "expired": "12/12/202",
        "layout": null,
        "created_at": "2019:10:12 19:08:43",
        "updated_at": null
    },
    {
        "id": "2",
        "batch": "ABC1223",
        "product": null,
        "expired": null,
        "layout": null,
        "created_at": "2019:10:06 09:32:14",
        "updated_at": null
    }
]
```

### **UPDATING AN OJBECT**
*POST* : `/update/{id}`


Required Parameters:
```
token: int
```
Optional Parameters:
```
batch: string
product: string
expired: string
layout: string
```
Example Responses:
```json
// Success
{
    "success": true,
    "msg": "Successfully update id: 11"
}

// Failed
{
    "success": false,
    "id": "18",
    "msg": "Failed to update id 18"
}
```
### **REMOVE AN OBJECT**
`/remove/{id}`

Required Parameters:
```
token: int
```
Example Responses:
```json
// Success
{
    "success": true,
    "id": "15",
    "msg": "Successfully remove id: 15"
}

// Failed
{
    "success": false,
    "id": "15",
    "msg": "Failed to remove id 15"
}
```


# **ABOUT FRAMEWORK**
# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
